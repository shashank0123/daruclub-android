package com.saumya.clubwinefinder.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import com.saumya.clubwinefinder.ModelClass.User;

public class SharedPrefManager {

    //-------------------This class is used to make methods to save the usr, check if user is logged in or not , getuser()  method that give all user details , clear() method to clear all existing data.-------------------------------
    private static final String SHARED_PREF_NAME="my_shared_preff";
    private static SharedPrefManager mInstance;

    private Context mCtx;

   private SharedPrefManager(Context mCtx) {
        this.mCtx = mCtx;
    }
      //------------------method to get instance of sharedprefmanager-----------------------
    public  static synchronized SharedPrefManager getInstance(Context mCtx){
        if (mInstance==null){
            mInstance=new SharedPrefManager(mCtx);
        }
        return mInstance;
    }

    public void saveLocation(Location location){
        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE );
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putLong("Latitude", (long) location.getLatitude());
    }
    public void saveUser(String mobile){
        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE );
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("mobile", mobile);
        editor.apply();
    }

    public boolean isLoggedIn(){
       SharedPreferences sharedPreferences= mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
       return sharedPreferences.getInt("id" , -1) != -1;
    }

    public void setLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putInt("id", 2).commit();
//        return sharedPreferences.getInt("id" , 2) != -1;
    }

    public User getUser(){
       SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);

       return new User(
               sharedPreferences.getInt("id",-1),
               sharedPreferences.getString("mobile",null)
       );
    }

    public void savedevicetoken(String tokena) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("devicetoken", tokena);
        editor.apply();
    }

    public String getdevicetoken() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString("devicetoken", "");
    }


    public void savename(String tokena) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("namea", tokena);
        editor.apply();
    }

    public String getname() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString("namea", "");
    }

    public void savedtoken(String tokena) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("token", tokena);
        editor.apply();
    }

    public void saveMOBILE(String tokena) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("mobile", tokena);
        editor.apply();
    }

    public String getMOBILE() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString("mobile", "");
    }

    public void savephone(String tokena) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("phone", tokena);
        editor.apply();
    }

    public String getphone() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString("phone", "");
    }

    public String gettoken() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString("token", "");
    }

    public void  clear(){
       //---------------To clear all the user previous data stored--------------------------

        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }


}

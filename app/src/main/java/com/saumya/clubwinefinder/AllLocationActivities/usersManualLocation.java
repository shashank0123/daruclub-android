package com.saumya.clubwinefinder.AllLocationActivities;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.saumya.clubwinefinder.Activities.NavigationActivity;
import com.saumya.clubwinefinder.R;

public class usersManualLocation extends AppCompatActivity implements OnMapReadyCallback {

    private FusedLocationProviderClient fusedLocationClient;
    private GoogleMap mMap;
    private CameraPosition mCameraPosition;
    private Boolean mLocationPermissionGranted=true;
    private AlphaAnimation buttonClick = new AlphaAnimation(2f , 0.4f);
    private Location mLastKnownLocation;
    private TextView ManualConfirm;
    private final LatLng mDefaultLocation = new LatLng(-33.8523341, 151.2106085);
    private static final int DEFAULT_ZOOM = 15;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //retrieve location using savedinstance
        if (savedInstanceState!=null){
            mLastKnownLocation=savedInstanceState.getParcelable("key_position");
            mCameraPosition=savedInstanceState.getParcelable("camera_position");
        }

        setContentView(R.layout.activity_users_manual_location);
        ManualConfirm=findViewById(R.id.btnmanualcnf);
        Places.initialize(this,"AIzaSyCJn449UVAwPL7LN4gi7k53trS1DwozCxs");
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.newMap);
        mapFragment.getMapAsync(this);

        PlacesClient placesClient = Places.createClient(this);
        ManualConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext() , "Location Confirmed " , Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getBaseContext() , NavigationActivity.class));
            }
        });

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(place.getLatLng()).title(place.getName().toString()));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 12.0f));
            }

            @Override
            public void onError(Status status) {

            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap=googleMap;
        updateLocationUI();
        getDeviceLocation();

    }

    private void getDeviceLocation() {
        try{
            if (mLocationPermissionGranted){
                Task<Location> locationResult= fusedLocationClient.getLastLocation();
                //locationResult contain all the information about last location
                locationResult.addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()){

                        mLastKnownLocation= task.getResult();
                        if (mLastKnownLocation != null) {
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude() , mLastKnownLocation.getLongitude() ), DEFAULT_ZOOM));

                        }
                    }else{
                        Log.d("TAG", "Current location is null. Using defaults.");
                        Log.e("TAG", "Exception: %s", task.getException());
                        mMap.moveCamera(CameraUpdateFactory
                                .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                        mMap.getUiSettings().setMyLocationButtonEnabled(false);
                    }
                });
            }
        }catch (SecurityException e){
            e.printStackTrace();
        }

    }

    private void updateLocationUI() {
        if (mMap==null){
            return;
        }
        else{
            try{
                if (mLocationPermissionGranted == true){
                    //Permission Granted-----------
                    mMap.setMyLocationEnabled(true);
                    mMap.getUiSettings().setMyLocationButtonEnabled(true);
                }else {
                    mMap.setMyLocationEnabled(false);
                    mMap.getUiSettings().setMyLocationButtonEnabled(false);
                    mLastKnownLocation=null;
                }
            }catch (SecurityException e){
                Log.e("Exception: %s", e.getMessage());
                e.printStackTrace();
            }
        }
    }
}

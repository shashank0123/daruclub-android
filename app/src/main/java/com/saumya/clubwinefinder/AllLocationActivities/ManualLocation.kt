package com.saumya.clubwinefinder.AllLocationActivities

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.animation.AlphaAnimation
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.saumya.clubwinefinder.Activities.NavigationActivity
import com.saumya.clubwinefinder.R
import kotlinx.android.synthetic.main.activity_manual_location.*

class ManualLocation : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {


    private val FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION
    private val COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION
    private val LOCATION_PERMISSION_REQUEST_CODE = 1234
    private val DEFAULT_ZOOM = 15f
    private var mLocationPermissionsGranted = false
    @Nullable
    private var mMap: GoogleMap? = null
    private var mFusedLocationProviderClient: FusedLocationProviderClient? = null
    private val buttonClick = AlphaAnimation(2f, 0.4f)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manual_location)

        cardconfirm.isFocusable=false
        btnmanualcnf.isFocusable=false

        //---------------------getting location permissions--------------------------------
        getLocationPermission()

        //------------------------getting the device location------------------------------------
        getDeviceLocation()

        iconconfirm.setOnClickListener {
            it.startAnimation(buttonClick)
        }
        btnmanualcnf.setOnClickListener {
            Toast.makeText(baseContext, "Location Confirmed", Toast.LENGTH_SHORT).show()
            startActivity(Intent(baseContext, NavigationActivity::class.java))
        }
    }

    override fun onConnectionFailed(p0: ConnectionResult) {}

    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap?) {
        Log.d("TAG", "onMapReady: map is ready")
        mMap = p0
        if (mLocationPermissionsGranted) {
            getDeviceLocation()
//            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
//                    this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
//            {
//
//            }
            mMap?.isMyLocationEnabled = true
            mMap?.isMyLocationEnabled = true
            mMap?.uiSettings?.isMyLocationButtonEnabled = false

            init()

        }

    }

    private fun init() {
        //------------------deals with the search bar--------------------------------
        SearchEdit.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE ||
                event?.action == KeyEvent.ACTION_DOWN || event?.action == KeyEvent.KEYCODE_ENTER
            ) {
                //--------------------------method for getting the address entered by user on Map---------------------------
                geoLocate()
            }
            false
        }
    } //init() ends here

    private fun geoLocate() {
        Log.d("TAG", " geoLocate: Geo Locating")
        val searchedloc: String = SearchEdit.text.toString()
        val geocoder = Geocoder(this)
        var list: List<Address>? = ArrayList()
        try {

            list = geocoder.getFromLocationName(searchedloc, 5)
        } catch (e: Exception) {

            Log.e("TAG", "geoLocate: IOException: " + e.message)

        }
        if (list!!.isNotEmpty()) {
            val address: Address = list[0]
            Log.d("TAG", "geoLocate: found a location: $address")

        }
    }

    private fun getDeviceLocation() {
        Log.d("TAG", "getDeviceLocation: getting the devices current location")
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        try {
            if (mLocationPermissionsGranted) {
                val location = mFusedLocationProviderClient?.lastLocation
                location?.addOnCompleteListener(this) {
                    if (it.isSuccessful) {
                        val currentLocation: Location = it.result!!
                        moveCamera(LatLng(currentLocation.latitude, currentLocation.longitude), DEFAULT_ZOOM)
                    } else {
                        Log.d("TAG", "onComplete: current location is null")
                        Toast.makeText(this, "unable to get current location", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        } catch (e: SecurityException) {
            Log.e("TAG", "getDeviceLocation: SecurityException: " + e.message)
        }
    }

    private fun getLocationPermission() {
        Log.d("TAG", "getLocationPermission: getting location permissions")
        val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)

        if (ContextCompat.checkSelfPermission(this.applicationContext, FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            if (ContextCompat.checkSelfPermission(this.applicationContext, COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            { mLocationPermissionsGranted = true
                initMap()
            } else {
                ActivityCompat.requestPermissions(this, permissions, LOCATION_PERMISSION_REQUEST_CODE)
                //after getting permission map should be initialized?----------------------------
                initMap()
            }
        } else {
            ActivityCompat.requestPermissions(this, permissions, LOCATION_PERMISSION_REQUEST_CODE)
            //after getting permission map should be initialized?--------------------------------
            initMap()
        }
    }

    private fun moveCamera(latLng: LatLng, zoom: Float) {
        Log.d("TAG", "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude)
        mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom))
    }

    private fun initMap() {
        Log.d("TAG", "initMap: initializing map")
        val mapFragment = supportFragmentManager.findFragmentById(R.id.manualmap) as SupportMapFragment?
        mapFragment!!.getMapAsync(this@ManualLocation)
        //OnmapReady is called from here
        //getmapasync gives a callback ---------------------
    }


}

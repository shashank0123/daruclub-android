package com.saumya.clubwinefinder.AllLocationActivities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.Task;
import com.saumya.clubwinefinder.Activities.NavigationActivity;
import com.saumya.clubwinefinder.R;

public class UsersCurrentLocation extends AppCompatActivity implements OnMapReadyCallback {

    private FusedLocationProviderClient fusedLocationClient;
    private  GoogleMap mMap;
    private CameraPosition mCameraPosition;
    private Location mLastKnownLocation;

    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";
    private Boolean mLocationPermissionGranted=true;
    String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    Integer My_Permission_Request_Code = 21;
    TextView CurrentLoc , ManualLoc;
    // A default location (Sydney, Australia) and default zoom to use when location permission is
    // not granted.
    private final LatLng mDefaultLocation = new LatLng(-33.8523341, 151.2106085);
    private static final int DEFAULT_ZOOM = 15;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    // Used for selecting the current place.
    private static final int M_MAX_ENTRIES = 5;
    private String[] mLikelyPlaceNames;
    private String[] mLikelyPlaceAddresses;
    private String[] mLikelyPlaceAttributions;
    private LatLng[] mLikelyPlaceLatLngs;
    private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.2F);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //retrieve location using savedinstance
        if (savedInstanceState!=null){
            mLastKnownLocation=savedInstanceState.getParcelable("key_position");
            mCameraPosition=savedInstanceState.getParcelable("camera_position");
        }

        setContentView(R.layout.activity_cuurent_loc);
        CurrentLoc=findViewById(R.id.btncurrentloc);
        ManualLoc=findViewById(R.id.manuallocation);
        //Location Services Client Created
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        //Build the Map
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        CurrentLoc.setOnClickListener(v -> {
            v.startAnimation(buttonClick);
            Toast.makeText(UsersCurrentLocation.this, "Current Location Confirmed", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getBaseContext(), NavigationActivity.class));
        });
//        ManualLoc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                v.startAnimation(buttonClick);
////                Toast.makeText(UsersCurrentLocation.this, "", Toast.LENGTH_SHORT).show();
//                Intent i=new Intent(getApplicationContext(), usersManualLocation.class);
//                startActivity(i);
//            }
//        });
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                //---------------------------Permission is not granted so we request permission using the request permission method------------------------------
//                //options in request permission depending on users mood------------------
//                mLocationPermissionGranted=false;
//                userDeniedPermission = shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION);
//
//                Log.d("Denied Loc Permission", userDeniedPermission.toString());
//                if (userDeniedPermission) {
//                    //User cancels to give permission repeatedly ----------------------------
//                    //User frustated ---------------------------------------------
//                    //Needs an explanation---------------------------------
//                    //Explanation can be shown using AlertDialogue-------------------
//                    Toast.makeText(this, " Please provide the permissions to Proceed.It will not harm your device", Toast.LENGTH_LONG).show();
//                    requestPermissions(permissions, My_Permission_Request_Code);
//
//                } else {
//                    //User cancels permission repeatedly and also press don't ask again----------------------------
//                    //we can't give explanation forcefully ------------------------------
//                    // request the permission-------------------------------
//                    requestPermissions(permissions, My_Permission_Request_Code);
//                }
//            }
//            //else part of amin permission check---------------------------
//            //--------------What has benn granted Permission is granted successfully------------------------------
//            //------------------  call or start the map-----------------------------------
//           mLocationPermissionGranted=true;
//            return;
//        }
//get the last location of mobile using getlastlocation method
//        getDeviceLocation();
//
//        fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
//            @Override
//            public void onSuccess(Location location) {
//                if (location != null) {
//                    location.getLongitude();
//                    location.getLatitude();
//                    Log.e("User Location", " Latitude :" + location.getLatitude() + "Longtitude" + location.getLongitude());
//                    latlong = new LatLng(location.getLatitude(), location.getLongitude());
//                }
//            }
//        });
    }

    /**
     * Saves the state of the map when the activity is paused.
     */
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        if (mMap != null) {
//            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
//            outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
//            super.onSaveInstanceState(outState);
//        }
//    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap=googleMap;

        getLocationPermission();
        updateLocationUI();
        getDeviceLocation();



    }

   private void getLocationPermission(){
        try{
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION )== PackageManager.PERMISSION_GRANTED){
            mLocationPermissionGranted=true;

        }else {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION }, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

        }}catch (Exception e){
            e.printStackTrace();
        }
   }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mLocationPermissionGranted=false;
        if (requestCode==PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION){
            if (grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
               //Permission Granted------
                mLocationPermissionGranted=true;
            }
        }
        //the made will be called irrespective of user grants the permission or not--------
       updateLocationUI();
    }

    private void updateLocationUI(){
    if (mMap==null){
        return;
    }
    else{
    try{
        if (mLocationPermissionGranted == true){
            //Permission Granted-----------
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        }else {
            mMap.setMyLocationEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mLastKnownLocation=null;
            getLocationPermission();
        }
    }catch (SecurityException e){
        Log.e("Exception: %s", e.getMessage());
        e.printStackTrace();
    }
    }
    }

    private void getDeviceLocation(){
        try{
            if (mLocationPermissionGranted){
                Task<Location>  locationResult= fusedLocationClient.getLastLocation();
                //locationResult contain all the information about last location
                locationResult.addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()){

                  mLastKnownLocation= task.getResult();
                        if (mLastKnownLocation != null) {
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude() , mLastKnownLocation.getLongitude() ), DEFAULT_ZOOM));
                        }
                    }else{
                        Log.d("TAG", "Current location is null. Using defaults.");
                        Log.e("TAG", "Exception: %s", task.getException());
                        mMap.moveCamera(CameraUpdateFactory
                                .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                        mMap.getUiSettings().setMyLocationButtonEnabled(false);
                    }
                });
            }
        }catch (SecurityException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}


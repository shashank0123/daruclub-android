package com.saumya.clubwinefinder.fcm

import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.saumya.clubwinefinder.Activities.ActualLogin
import com.saumya.clubwinefinder.Activities.NavigationActivity
import com.saumya.clubwinefinder.storage.SharedPrefManager
import org.json.JSONObject


class MyFirebaseMessagingService : FirebaseMessagingService() {


    companion object {
        val STRING_TAG = MyFirebaseMessagingService::class.java.simpleName
    }

    val user = FirebaseAuth.getInstance().currentUser

    override fun onNewToken(s: String?) {
        super.onNewToken(s)
        Log.d(STRING_TAG, "token=" + s)
        if (user != null) {

            //add token here
        }

        addapi(s!!, this)

        Log.d(STRING_TAG, "token save in shared is ")

    }


    fun addapi(devicetoken: String, context: Context) {

        Log.d(STRING_TAG, "inside addapi")


        val tokena = SharedPrefManager.getInstance(context).gettoken()
        if (tokena.length > 10) {
            Log.d(STRING_TAG, "inside tokena")
            val regi_path = "https://daru.club/api/firebasetoken"
//        val requesta=Volley.newRequestQueue(this)
            val jsonabody = JSONObject()
            jsonabody.put("device_token", devicetoken)

            jsonabody.put("token", tokena)

            SharedPrefManager.getInstance(context).savedevicetoken(devicetoken)
            val requestbo = jsonabody.toString()
            // Request a string response from the provided URL.
            val stringRequest = object : StringRequest(
                Request.Method.POST, regi_path,
                com.android.volley.Response.Listener<String> { response ->
                    // Display the first 500 characters of the response string.
                    val jsons: JsonObject = JsonParser().parse(response).asJsonObject
                    Log.d(STRING_TAG, "json partser=" + jsons)

                    val dataa = jsons.get("success").asBoolean
                    val mess = jsons.get("message").asString

                    if (dataa) {
                        //-----------------success field returns true---------------------------
                        SharedPrefManager.getInstance(this).savedevicetoken(devicetoken)
                        val sd = SharedPrefManager.getInstance(this)
                        sd.savedevicetoken(devicetoken)

                    } else {
                        Toast.makeText(baseContext, mess, Toast.LENGTH_SHORT).show()
                    }


                },
                com.android.volley.Response.ErrorListener {
                    Log.d(STRING_TAG, "STRING error=" + it.localizedMessage)
                }) {

                override fun getBody(): ByteArray {
                    return requestbo.toByteArray()
                }

                override fun getBodyContentType(): String {
                    return "application/json"
                }


            }

            MySingleton.getInstance(context).addToRequestQueue(stringRequest)
        } else Log.d(STRING_TAG, "USER TOKEN NOT POSSIBLE TO UPLOADED= " + tokena)

    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)

    }

    override fun onMessageSent(p0: String?) {
        super.onMessageSent(p0)
    }

    override fun onDeletedMessages() {
        super.onDeletedMessages()
    }

    override fun onSendError(p0: String?, p1: Exception?) {
        super.onSendError(p0, p1)
    }


}

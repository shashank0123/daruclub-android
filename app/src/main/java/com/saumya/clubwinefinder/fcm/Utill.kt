package com.saumya.clubwinefinder.fcm

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager

import java.util.*
import android.os.Build
import android.preference.PreferenceManager
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import android.graphics.Bitmap
import android.content.res.Configuration
import android.graphics.Canvas
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.model.BitmapDescriptor


class Utillity(val context: Context) {

    companion object {
        val STRING_TAG = Utillity::class.java.simpleName
    }


    fun is_location_permission(resuquetcode: Int): Boolean {
        var a = false
        if (ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            ActivityCompat.requestPermissions(
                context as Activity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                resuquetcode
            )
        } else a = true

        return a
    }


}
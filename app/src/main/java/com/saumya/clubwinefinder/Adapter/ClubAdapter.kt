package com.saumya.clubwinefinder.Adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonArray
import com.saumya.clubwinefinder.Activities.BarDetailActivity
import com.saumya.clubwinefinder.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.club_row.view.*

class ClubAdapter(val context: Context, val list: JsonArray) : RecyclerView.Adapter<ClubAdapter.ClubHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClubHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.club_row, parent, false)
        return ClubHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size()
    }

    override fun onBindViewHolder(holder: ClubHolder, position: Int) {
        val current = list.get(position).asJsonObject

        holder.tbarname.text = current.get("name").asString
        try {
            Picasso.get().load(current.get("image").asString).into(holder.barimage)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        holder.tvstar.text = current.get("rating").asString + " starred"
        holder.bdetails.setOnClickListener {
            val intenta = Intent(context, BarDetailActivity::class.java)
            intenta.putExtra("id", current.get("id").toString())
            context.startActivity(intenta)
        }

        val rating = current.get("rating").asString.toInt()

        when (rating) {

            1 -> {
                holder.image1.setImageDrawable(context.getDrawable(R.drawable.greenstar))
            }

            2 -> {
                holder.image1.setImageDrawable(context.getDrawable(R.drawable.greenstar))
                holder.imagestar2.setImageDrawable(context.getDrawable(R.drawable.greenstar))
            }

            3 -> {
                holder.image1.setImageDrawable(context.getDrawable(R.drawable.greenstar))
                holder.imagestar2.setImageDrawable(context.getDrawable(R.drawable.greenstar))
                holder.imagestar3.setImageDrawable(context.getDrawable(R.drawable.greenstar))
            }

            4 -> {
                holder.image1.setImageDrawable(context.getDrawable(R.drawable.greenstar))
                holder.imagestar2.setImageDrawable(context.getDrawable(R.drawable.greenstar))
                holder.imagestar3.setImageDrawable(context.getDrawable(R.drawable.greenstar))
                holder.imagestar4.setImageDrawable(context.getDrawable(R.drawable.greenstar))
            }

            5 -> {
                holder.image1.setImageDrawable(context.getDrawable(R.drawable.greenstar))
                holder.imagestar2.setImageDrawable(context.getDrawable(R.drawable.greenstar))
                holder.imagestar3.setImageDrawable(context.getDrawable(R.drawable.greenstar))
                holder.imagestar4.setImageDrawable(context.getDrawable(R.drawable.greenstar))
                holder.imagestar5.setImageDrawable(context.getDrawable(R.drawable.greenstar))
            }
        }


    }

    class ClubHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tbarname = itemView.barname
        val constabars = itemView.constbars
        val barimage = itemView.imagebar
        val image1 = itemView.imagestar1
        val imagestar2 = itemView.imagestar2
        val imagestar3 = itemView.imagestar3
        val imagestar4 = itemView.imagestar4
        val imagestar5 = itemView.imagestar5
        val bdetails = itemView.btnclub
        val tvstar = itemView.tvstars

    }

}
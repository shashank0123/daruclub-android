package com.saumya.clubwinefinder.Adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.saumya.clubwinefinder.Activities.ActualLogin
import com.saumya.clubwinefinder.Activities.ItemDetailActivity
import com.saumya.clubwinefinder.ModelClass.MenuBarsItem
import com.saumya.clubwinefinder.R
import com.saumya.clubwinefinder.fcm.MySingleton
import com.saumya.clubwinefinder.storage.SharedPrefManager
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.itemrow.view.*
import org.json.JSONObject

class ItemDetailAdapter(val context: Context, val itemlist: JsonArray) :
    RecyclerView.Adapter<ItemDetailAdapter.ItemHolder>() {

    companion object {
        val STRING_TAG = ItemDetailAdapter::class.java.simpleName
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {

        val view=LayoutInflater.from(context).inflate(R.layout.itemrow, parent , false)
        return  ItemHolder(view)
    }

    override fun getItemCount(): Int {
        return itemlist.size()
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val currentmenu = itemlist.get(position).asJsonObject
        ItemDetailActivity.hashmapa.put(position, 0)
 with(holder.itemView){
     Log.d("ItemDetail Adapter", currentmenu.get("name").toString())
     Picasso.get().load(currentmenu.get("image").asString).into(circleimage)
     namelast.text = currentmenu.get("name").asString
     pricelast.text = currentmenu.get("price").toString()

     bminus.setOnClickListener {
         val currentpos = bshow.text.toString().toInt()
         if (currentpos > 1) {
             bshow.text = String.format("%02d", (currentpos - 1))

         } else {
             Toast.makeText(context, "Can't be less than 1", Toast.LENGTH_LONG).show()

         }
     }

     badd.setOnClickListener {
         val currentpos = bshow.text.toString().toInt()
         ItemDetailActivity.totala = ItemDetailActivity.totala + 1

         bshow.text = String.format("%02d", (currentpos + 1))


         val regi_path = "https://daru.club/api/savebooking"

         val jsonabody = JSONObject()
         val tokanana = SharedPrefManager.getInstance(context).gettoken()
         Log.d(ItemDetailActivity.STRING_TAG, "tokena=" + tokanana)
         jsonabody.put("token", tokanana)
         jsonabody.put("bar_id", currentmenu.get("bar_id"))
         jsonabody.put("no_of_item", currentpos + 1)
         jsonabody.put("item_id", currentmenu.get("id"))
         var aa = ""
         val vak = ItemDetailActivity.hashamaid.keys
         for (i in 0..vak.size - 1) {
             Log.d(STRING_TAG, "keya=" + vak.elementAt(i))
             val va = ItemDetailActivity.hashamaid.get(vak.elementAt(i))
             Log.d(ItemDetailActivity.STRING_TAG, "va=" + va)
             if (va == 1) {
                 aa = aa + va + ","
             }
         }

         jsonabody.put("item_ids", aa)
         Log.d(STRING_TAG, "aa=" + aa)


         val requestbo = jsonabody.toString()
         // Request a string response from the provided URL.
         val stringRequest = object : StringRequest(
             Request.Method.POST, regi_path,
             com.android.volley.Response.Listener<String> { response ->
                 // Display the first 500 characters of the response string.
                 val jsons: JsonObject = JsonParser().parse(response).asJsonObject
                 Log.d(STRING_TAG, "json partser=" + jsons)

                 val dataa = jsons.get("status").asInt
                 val mess = jsons.get("message").asString
                 val booking = jsons.get("booking").asJsonObject
                 val ida = booking.get("id").asInt
                 Log.d(STRING_TAG, "id-=" + ida)

                 if (dataa == 200) {


                 } else {

                 }
//                 Toast.makeText(baseContext, mess, Toast.LENGTH_SHORT).show()

             },
             com.android.volley.Response.ErrorListener {
                 Log.d(STRING_TAG, "STRING error=" + it.localizedMessage)
             }) {

             override fun getBody(): ByteArray {
                 return requestbo.toByteArray()
             }

             override fun getBodyContentType(): String {
                 return "application/json"
             }

         }

         MySingleton.getInstance(context).addToRequestQueue(stringRequest)


     }

     constaselectaaa.setOnClickListener {

         //         val cc = ItemDetailActivity.hashmapa.get(position)
//
//         if (cc == 0) {
//             constaselectaaa.setBackgroundColor(context.getColor(R.color.greyselect))
//             ItemDetailActivity.hashmapa.put(position, 1)
//             Log.d(ItemDetailActivity.STRING_TAG, "CURRECT ID=" + currentmenu.get("id").asInt)
//             ItemDetailActivity.hashamaid.put(position, currentmenu.get("id").asInt)
//         } else {
//             constaselectaaa.setBackgroundColor(context.getColor(R.color.greylight))
//             ItemDetailActivity.hashmapa.put(position, 0)
//             ItemDetailActivity.hashamaid.remove(position)
//         }


     }
 }





    }

    class ItemHolder(itemView :View) : RecyclerView.ViewHolder(itemView) {

        val bminus = itemView.bminus
        val bpluss = itemView.badd
        val bshow = itemView.bshow

    }

}
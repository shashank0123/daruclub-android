package com.saumya.clubwinefinder.Adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonArray
import com.saumya.clubwinefinder.Activities.NavigationActivity
import com.saumya.clubwinefinder.Activities.BarActivity
import com.saumya.clubwinefinder.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.menurow.view.*

class DrinkTypeAdapter(val context: Context, val menulist: JsonArray) :
    RecyclerView.Adapter<DrinkTypeAdapter.MenuHolder>() {


    //Merchant key: zhr8UvQn
    //Merchant Salt: PSz3qDmg8a

    //https://www.getpostman.com/collections/39ee1fc3d182490c7f33
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.menurow, parent, false)
        return MenuHolder(view)

    }

    override fun getItemCount(): Int {
        return menulist.size()
    }

    override fun onBindViewHolder(holder: MenuHolder, position: Int) {
        val currentmenu = menulist.get(position).asJsonObject
        holder.itemname.text = currentmenu.get("name").asString

        holder.itemimag.setOnClickListener {
            Log.d("INSISE", "INSIDE")
            val id = currentmenu.get("id").toString()
            Log.d("INSISE", "ID=" + id)
            Log.d("INSISE", "context=" + context)
            val intenta = Intent(context, BarActivity::class.java)
            intenta.putExtra("id", id)
            intenta.putExtra("lati", NavigationActivity.lati)
            intenta.putExtra("longi", NavigationActivity.longi)
            context.startActivity(intenta)
        }

        Picasso.get().load(currentmenu.get("image").asString).into(holder.itemimag)

    }

    class MenuHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val itemimag = itemView.itemimage1
        val itemname = itemView.itembeer

    }


}
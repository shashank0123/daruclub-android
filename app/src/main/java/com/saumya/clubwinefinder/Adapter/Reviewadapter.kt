package com.saumya.clubwinefinder.Adapter

import android.content.Context
import android.content.Intent

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonArray
import com.saumya.clubwinefinder.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.reviewcustom.view.*


class ReviewadapterInflater(val context: Context, val jsona: JsonArray) :
    RecyclerView.Adapter<ReviewadapterInflater.viewholder>() {
    init {
        //query data here
    }

    companion object {
        val STRING_TAG = ReviewadapterInflater::class.java.name
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewholder {

        val view: View = LayoutInflater.from(context).inflate(R.layout.reviewcustom, parent, false)
        return viewholder(view)
    }

    override fun onBindViewHolder(holder: viewholder, position: Int) {
        //Binding data

        val cureenta = jsona.get(position).asJsonObject

        with(holder.itemView) {
            val imageaa = cureenta.has("image")
            if (imageaa) {
                Picasso.get().load(cureenta.get("image").asString).into(im_review)
            }

            val rreciewa = cureenta.get("review").asString

            treviews.text = rreciewa
        }
    }

    override fun getItemCount(): Int {

        return jsona.size()
    }

    class viewholder(view: View) : RecyclerView.ViewHolder(view) {


    }

}

package com.saumya.clubwinefinder.ModelClass;

public class User {

    private int id;
    private String mobile;

    public User(int id, String mobile) {
        this.id = id;
        this.mobile = mobile;
    }

    public int getId() {
        return id;
    }

    public String getMobile() {
        return mobile;
    }
}

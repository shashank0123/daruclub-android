package com.saumya.clubwinefinder.ModelClass

data class MenuBarsItem(val image: String = "",
                    val price: Int = 0,
                    val name: String = "",
                    val id: Int = 0)


data class MenuDetailResponse(val message: String = "",
                              val bars: List<MenuBarsItem>?,
                              val status: Int = 0)



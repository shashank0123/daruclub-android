package com.saumya.clubwinefinder.ModelClass


data class BarsItem(val image: String = "",
                    val name: String = "",
                    val rating: String = "")

data class Barlist(val message: String = "",
                   val bars: List<BarsItem>?,
                   val status: Int = 0)



package com.saumya.clubwinefinder.ModelClass

data class Reviews(val barId: String = "",
                   val updatedAt : String="",
                   val userId: String = "",
                   val review: String = "",
                   val rating: String = "",
                   val createdAt :String="",
                   val id: Int = 0)


data class BardetailModel(val barDetails: BarDetails,
                          val reviews: Reviews,
                          val message: String = "",
                          val status: Int = 0)


data class BarDetails(val image: String = "",
                      val updatedAt: String = "",
                      val name: String = "",
                      val rating: String = "",
                      val createdAt: String = "",
                      val openTime: String = "",
                      val id: Int = 0,
                      val barDesc: String = "",
                      val totalSeats: String = "",
                      val status: String = "")



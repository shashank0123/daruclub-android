package com.saumya.clubwinefinder.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.saumya.clubwinefinder.R
import com.saumya.clubwinefinder.fcm.MyFirebaseMessagingService
import com.saumya.clubwinefinder.fcm.MySingleton
import com.saumya.clubwinefinder.storage.SharedPrefManager
import kotlinx.android.synthetic.main.activity_otp_validate.*
import org.json.JSONObject

class OtpValidateActivity : AppCompatActivity() {

    companion object {
        val STRING_TAG = OtpValidateActivity::class.java.simpleName
    }

    var namea = "0"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_validate)

        val mobilenumber = intent.getStringExtra("phone")
        namea = intent.getStringExtra("name")
        if (namea == "0") {
            etname.visibility = View.GONE
        }
        btnvalidate.setOnClickListener {
            val otp = etotp.text.toString()
            val name = etname.text.toString()

            if (otp!!.length == 0) {
                if (otp.length == 0) {
                    etotp.error = "Enter Otp"
                } else etname.error = "Enter Name"

            } else {


                val regi_path = "https://daru.club/api/validate_otp"
                val detokena = SharedPrefManager.getInstance(this).getdevicetoken()
                val jsonabody = JSONObject()
                jsonabody.put("mobile", mobilenumber)
                jsonabody.put("otp", otp)
                if (namea != "0") {
                    jsonabody.put("name", name)
                }

                SharedPrefManager.getInstance(this).savename(name)
//                SharedPrefManager.getInstance(this).saveMOBILE(mobilenumber)
                if (detokena.length > 5) {
                    jsonabody.put("device_token", detokena)
                }

                val requestbo = jsonabody.toString()
                // Request a string response from the provided URL.
                Log.d(STRING_TAG, "requestbo=" + requestbo)
                val stringRequest = object : StringRequest(
                    Request.Method.POST, regi_path,
                    com.android.volley.Response.Listener<String> { response ->
                        // Display the first 500 characters of the response string.
                        val jsons: JsonObject = JsonParser().parse(response).asJsonObject
                        Log.d(ActualLogin.STRING_TAG, "json partser=" + jsons)

                        val dataa = jsons.get("success").asBoolean
                        val mess = jsons.get("message").asString

                        if (dataa) {
                            //-----------------success field returns true---------------------------
                            SharedPrefManager.getInstance(this).saveUser(name.toString())
                            val user = jsons.getAsJsonObject("user")
                            Log.d(STRING_TAG, "users=" + user)
                            val token = user.get("token").asString
                            Log.d(STRING_TAG, "token" + token)
                            SharedPrefManager.getInstance(this).savedtoken(token)
                            SharedPrefManager.getInstance(this).setLoggedIn()
                            val detoken = SharedPrefManager.getInstance(this).getdevicetoken()
                            MyFirebaseMessagingService().addapi(detoken, this)

                            startActivity(Intent(baseContext, NavigationActivity::class.java))
                        } else {
                            Toast.makeText(baseContext, mess, Toast.LENGTH_SHORT).show()
                        }


                    },
                    com.android.volley.Response.ErrorListener {
                        Log.d(STRING_TAG, "STRING error=" + it.localizedMessage)
                    }) {


                    override fun getBody(): ByteArray {
                        return requestbo.toByteArray()
                    }

                    override fun getBodyContentType(): String {
                        return "application/json"
                    }
                }

                MySingleton.getInstance(this).addToRequestQueue(stringRequest)


            }


        }
    }
}

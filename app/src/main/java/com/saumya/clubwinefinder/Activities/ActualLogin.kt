package com.saumya.clubwinefinder.Activities

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.animation.AlphaAnimation
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.libraries.places.internal.it
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.saumya.clubwinefinder.AllLocationActivities.UsersCurrentLocation
import com.saumya.clubwinefinder.ModelClass.LoginResponse
import com.saumya.clubwinefinder.R
import com.saumya.clubwinefinder.RetrofitApi.ApiClient
import com.saumya.clubwinefinder.fcm.MySingleton
import com.saumya.clubwinefinder.storage.SharedPrefManager
import kotlinx.android.synthetic.main.activity_actual_login.*
import org.json.JSONObject
import java.nio.charset.Charset


class ActualLogin : AppCompatActivity() {


    companion object {
        val STRING_TAG = ActualLogin::class.java.simpleName
    }

    var phone: String = null.toString()
    @SuppressLint("NewApi")
    private val buttonClick = AlphaAnimation(1f, 0.4f)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.saumya.clubwinefinder.R.layout.activity_actual_login)
        btnLog.isFocusable=false
        btnLog.isClickable=false
        btnSignup.isFocusable = false
        btnSignup.isClickable = false

        btnLog.setOnClickListener {
            it.startAnimation(buttonClick)
            btnLog.isFocusable=true
            btnLog.setBackgroundColor(Color.GREEN)
            signIn()
        }
        btnSignup.setOnClickListener {
            it.startAnimation(buttonClick)
            btnSignup.isFocusable = true
            btnSignup.setBackgroundColor(getColor(R.color.colorAccent))
            startActivity(Intent(baseContext, LoginActivity::class.java))
        }
    }

//    override fun onStart() {
//        super.onStart()
//        if (SharedPrefManager.getInstance(this).isLoggedIn) {
//            //--------------------------------if the user is already logged in-------------------------------
//            val intent = Intent(this, UsersCurrentLocation::class.java)
//            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//            startActivity(intent)
//        }
//    }

    private fun signIn() {

        phone = editphone.text.toString()
        if (phone.isEmpty()) {
            editphone.error = "Phone Number Is Required!"
            editphone.requestFocus()
            return
        }else
        if (!Patterns.PHONE.matcher(phone).matches()) {
            editphone.error = "Enter Valid Phone Number"
            editphone.requestFocus()
            return
        }else
        if (phone.length < 10) {
            editphone.error = "Phone Number Should Be 10 digit long"
            return
        }

        val regi_path = "https://daru.club/api/register"
        val jsonabody = JSONObject()
        jsonabody.put("mobile", phone)
        val requestbo = jsonabody.toString()
        Log.d(STRING_TAG, "res body=" + requestbo)
        // Request a string response from the provided URL.
        val stringRequest = object : StringRequest(
            Request.Method.POST, regi_path,
            com.android.volley.Response.Listener<String> { response ->
                // Display the first 500 characters of the response string.
                val jsons: JsonObject = JsonParser().parse(response).asJsonObject
                Log.d(STRING_TAG, "json partser=" + jsons)

                val dataa = jsons.get("success").asBoolean
                val mess = jsons.get("message").asString
                val nama = jsons.get("name").asString

                if (dataa) {
                    //-----------------success field returns true---------------------------
                    SharedPrefManager.getInstance(this@ActualLogin).saveUser(nama)
                    SharedPrefManager.getInstance(this).setLoggedIn()
                    val intent = Intent(baseContext, NavigationActivity::class.java)
                    Toast.makeText(this@ActualLogin, mess, Toast.LENGTH_SHORT).show()
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                } else {
                    Toast.makeText(baseContext, mess, Toast.LENGTH_SHORT).show()
                }


            },
            Response.ErrorListener {
                Log.d(STRING_TAG, "STRING error=" + it.localizedMessage)
            }) {


            override fun getBody(): ByteArray {
                return requestbo.toByteArray()
            }

            override fun getBodyContentType(): String {
                return "application/json"
            }

        }

        MySingleton.getInstance(this).addToRequestQueue(stringRequest)



    }

    override fun onBackPressed() {
        AlertDialog.Builder(this)
            .setTitle("Really Exit?")
            .setMessage("Are you sure you want to exit?")
            .setNegativeButton(android.R.string.no, null)
            .setPositiveButton(android.R.string.yes, object : DialogInterface.OnClickListener{
                override fun onClick(dialog: DialogInterface?, which: Int) {

                    onBackPressed()
                }
            })
            .create()
            .show()
        finish()
        super.onBackPressed()

    }

    }


package com.saumya.clubwinefinder.Activities

import android.content.Intent
import android.os.Bundle
import android.view.animation.AlphaAnimation
import androidx.appcompat.app.AppCompatActivity
import com.saumya.clubwinefinder.Fragments.BarFragment
import com.saumya.clubwinefinder.R
import kotlinx.android.synthetic.main.activity_search_tabbed.*

class SearchTabbedActivity : AppCompatActivity() {

    private val alphaAnimation=AlphaAnimation(0.2f,2f)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_tabbed)
        showfrag()

        daruslist.setOnClickListener {
            it.startAnimation(alphaAnimation)
            it.setBackgroundResource(R.drawable.changebuttonshape)
            showfrag()
        }
        barslist.setOnClickListener {
            it.startAnimation(alphaAnimation)
            it.setBackgroundResource(R.drawable.changebuttonshape)
            startActivity(Intent(baseContext, BarActivity::class.java ))
        }

    }
    private fun showfrag(){
        supportFragmentManager.beginTransaction().replace(R.id.frame, BarFragment()).commit()
    }
}

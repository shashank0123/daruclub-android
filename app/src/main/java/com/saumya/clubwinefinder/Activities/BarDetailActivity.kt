package com.saumya.clubwinefinder.Activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.saumya.clubwinefinder.Adapter.ClubAdapter
import com.saumya.clubwinefinder.Adapter.ReviewadapterInflater
import com.saumya.clubwinefinder.ModelClass.BarDetails
import com.saumya.clubwinefinder.R
import com.saumya.clubwinefinder.RetrofitApi.ApiClient
import com.saumya.clubwinefinder.fcm.MySingleton
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_bar.*
import kotlinx.android.synthetic.main.activity_bar_detail.*
import retrofit2.Call
import retrofit2.Response

class BarDetailActivity : AppCompatActivity() {

    companion object {
        val STRING_TAG = BarDetailActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bar_detail)
        val id = intent.getStringExtra("id")
        Log.d(STRING_TAG, "id=" + id)
        val barurl = "https://daru.club/api/bardetails?bar_id=" + id

        rec_review.setHasFixedSize(true)
        rec_review.layoutManager = LinearLayoutManager(this)


        val hashMap = HashMap<String, String>()
        hashMap.put("bar_id", id)

        val stringRequest = object : StringRequest(
            Request.Method.GET, barurl,
            com.android.volley.Response.Listener<String> { response ->
                // Display the first 500 characters of the response string.
                val jsons: JsonObject = JsonParser().parse(response).asJsonObject
                Log.d(STRING_TAG, "json partser=" + jsons)

                val startus = jsons.get("status").asInt

                if (startus == 200) {
                    val bar_details = jsons.get("bar_details").asJsonObject
                    val reviews = jsons.get("reviews").asJsonArray

                    Log.d(STRING_TAG, "reviews=" + reviews)

                    BarName.text = bar_details.get("name").asString
                    Picasso.get().load(bar_details.get("image").asString).into(BarImage)

                    val rating = bar_details.get("rating").asString
                    tvstars.text = rating + " Starred"

                    rec_review.adapter = ReviewadapterInflater(this, reviews)
                    when (rating.toInt()) {

                        1 -> {
                            imagestar1.setImageDrawable(getDrawable(R.drawable.greenstar))

                        }

                        2 -> {
                            imagestar1.setImageDrawable(getDrawable(R.drawable.greenstar))
                            imagestar2.setImageDrawable(getDrawable(R.drawable.greenstar))
                        }

                        3 -> {
                            imagestar1.setImageDrawable(getDrawable(R.drawable.greenstar))
                            imagestar2.setImageDrawable(getDrawable(R.drawable.greenstar))
                            imagestar3.setImageDrawable(getDrawable(R.drawable.greenstar))

                        }

                        4 -> {
                            imagestar1.setImageDrawable(getDrawable(R.drawable.greenstar))
                            imagestar2.setImageDrawable(getDrawable(R.drawable.greenstar))
                            imagestar3.setImageDrawable(getDrawable(R.drawable.greenstar))
                            imagestar4.setImageDrawable(getDrawable(R.drawable.greenstar))
                        }

                        5 -> {
                            imagestar1.setImageDrawable(getDrawable(R.drawable.greenstar))
                            imagestar2.setImageDrawable(getDrawable(R.drawable.greenstar))
                            imagestar3.setImageDrawable(getDrawable(R.drawable.greenstar))
                            imagestar4.setImageDrawable(getDrawable(R.drawable.greenstar))
                            imagestar5.setImageDrawable(getDrawable(R.drawable.greenstar))
                        }
                    }

                    tvcheckmenu.visibility = View.VISIBLE


                }


            },
            com.android.volley.Response.ErrorListener {
                Log.d(STRING_TAG, "STRING error=" + it.localizedMessage)
            }) {
            override fun getParams(): MutableMap<String, String> {
                return hashMap
            }

        }

        MySingleton.getInstance(this).addToRequestQueue(stringRequest)





        tvcheckmenu.setOnClickListener {
            val intenta = Intent(baseContext, ItemDetailActivity::class.java)
            intenta.putExtra("id", id)
            startActivity(intenta)
        }
    }
}

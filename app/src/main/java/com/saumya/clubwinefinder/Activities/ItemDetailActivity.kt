package com.saumya.clubwinefinder.Activities

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.saumya.clubwinefinder.Adapter.ItemDetailAdapter
import com.saumya.clubwinefinder.ModelClass.MenuBarsItem
import com.saumya.clubwinefinder.R
import com.saumya.clubwinefinder.RetrofitApi.ApiClient
import com.saumya.clubwinefinder.fcm.MySingleton
import com.saumya.clubwinefinder.storage.SharedPrefManager
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_bar_detail.*
import kotlinx.android.synthetic.main.activity_item_detail.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import android.widget.TimePicker
import android.app.TimePickerDialog
import java.util.*
import kotlin.collections.HashMap


class ItemDetailActivity : AppCompatActivity(), View.OnClickListener {


    companion object {
        val STRING_TAG = ItemDetailActivity::class.java.simpleName
        val hashmapa = HashMap<Int, Int>()
        val hashamaid = HashMap<Int, Int>()
        var totala = 0
        var timea=""
    }

    var id = "1"
    var seats = "4"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_detail)

        id = intent.getStringExtra("id")

        rvitems.setHasFixedSize(true)
        rvitems.layoutManager=LinearLayoutManager(this)

        Log.d("AccountActivity", "phone in item detial=" + SharedPrefManager.getInstance(this).getphone())

        showdialog("","")

//        val hashMap = HashMap<String, String>()
//        hashMap.put("bar_id", id)
//        Log.d(STRING_TAG, "id=" + id)
//
//        val menuurl = "https://daru.club/api/menu_items?bar_id=" + id
//        val stringRequest = object : StringRequest(
//            Request.Method.GET, menuurl,
//            com.android.volley.Response.Listener<String> { response ->
//                // Display the first 500 characters of the response string.
//                val jsons: JsonObject = JsonParser().parse(response).asJsonObject
//                Log.d(BarDetailActivity.STRING_TAG, "json partser=" + jsons)
//
//                val startus = jsons.get("status").asInt
//
//                if (startus == 200) {
//                    val bars = jsons.get("bars").asJsonArray
//                    rvitems.adapter = ItemDetailAdapter(this, bars)
//
//                }
//
//
//            },
//            com.android.volley.Response.ErrorListener {
//                Log.d(STRING_TAG, "STRING error=" + it.localizedMessage)
//            }) {
//
//        }
//
//        MySingleton.getInstance(this).addToRequestQueue(stringRequest)


        bookNow.setOnClickListener(this)



    }


    override fun onClick(v: View?) {

        val regi_path = "https://daru.club/api/get_bill"

        val jsonabody = JSONObject()
        val tokanana = SharedPrefManager.getInstance(this).gettoken()
        Log.d(STRING_TAG, "tokena=" + tokanana)
        jsonabody.put("token", tokanana)
//        jsonabody.put("bar_id", id)
//        jsonabody.put("no_of_seat", seats)
//        var aa = ""
//        val vak = hashamaid.keys
//        for (i in 0..vak.size - 1) {
//            Log.d(STRING_TAG, "keya=" + vak.elementAt(i))
//            val va = hashamaid.get(vak.elementAt(i))
//            Log.d(STRING_TAG, "va=" + va)
//            if (va == 1) {
//                aa = aa + va + ","
//            }
//        }
//
//        jsonabody.put("item_ids", aa)
//        Log.d(STRING_TAG, "aa=" + aa)


        val requestbo = jsonabody.toString()
        // Request a string response from the provided URL.

        Log.d(STRING_TAG, "TOTLA=" + totala)
        if (totala > 0) {
            val stringRequest = object : StringRequest(
                Request.Method.POST, regi_path,
                com.android.volley.Response.Listener<String> { response ->
                    // Display the first 500 characters of the response string.
                    val jsons: JsonObject = JsonParser().parse(response).asJsonObject
                    Log.d(STRING_TAG, "json partser=" + jsons)

                    val dataa = jsons.get("status").asInt
                    val mess = jsons.get("message").asString

                    val billobject = jsons.get("bill").asJsonObject
                    val amount = billobject.get("amount").asInt
                    val email = billobject.get("email").asString
                    val productinfo = billobject.get("productinfo").asString
                    val firstname = billobject.get("firstname").asString
                    val user_credentials = billobject.get("user_credentials").asInt

                    if (dataa == 200) {

                        Toast.makeText(this, "party over . Wait for bill", Toast.LENGTH_LONG).show()

                        val intenta = Intent(this, AccountActivity::class.java)
                        intenta.putExtra("amount", amount)
                        intenta.putExtra("email", email)
                        intenta.putExtra("productinfo", productinfo)
                        intenta.putExtra("firstname", firstname)
                        intenta.putExtra("user_credentials", user_credentials)
                        startActivity(intenta)


                    } else {

                    }
//                Toast.makeText(baseContext, mess, Toast.LENGTH_SHORT).show()

                },
                com.android.volley.Response.ErrorListener {
                    Log.d(STRING_TAG, "STRING error=" + it.localizedMessage)
                }) {

                override fun getBody(): ByteArray {
                    return requestbo.toByteArray()
                }

                override fun getBodyContentType(): String {
                    return "application/json"
                }

            }
            MySingleton.getInstance(this).addToRequestQueue(stringRequest)
        } else {
            Toast.makeText(this, "Please add drinks", Toast.LENGTH_LONG).show()

        }


    }


    fun callmenuitem(){
        val hashMap = HashMap<String, String>()
        hashMap.put("bar_id", id)
        hashMap.put("token",SharedPrefManager.getInstance(this).gettoken())
        hashMap.put("time", timea)
        hashMap.put("date","")
        Log.d(STRING_TAG, "id=" + id)

        val menuurl = "https://daru.club/api/menu_items?bar_id=" + id
        val stringRequest = object : StringRequest(
            Request.Method.GET, menuurl,
            com.android.volley.Response.Listener<String> { response ->
                // Display the first 500 characters of the response string.
                val jsons: JsonObject = JsonParser().parse(response).asJsonObject
                Log.d(BarDetailActivity.STRING_TAG, "json partser=" + jsons)

                val startus = jsons.get("status").asInt

                if (startus == 200) {
                    val bars = jsons.get("bars").asJsonArray
                    rvitems.adapter = ItemDetailAdapter(this, bars)

                }


            },
            com.android.volley.Response.ErrorListener {
                Log.d(STRING_TAG, "STRING error=" + it.localizedMessage)
            }) {

        }

        MySingleton.getInstance(this).addToRequestQueue(stringRequest)
    }



    fun showdialog(mess:String,title:String){
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
        val minute = mcurrentTime.get(Calendar.MINUTE)
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(this,
            TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute ->

                timea=selectedHour.toString()+":"+selectedMinute

                callmenuitem()
            }, hour, minute, true
        )//Yes 24 hour time
        mTimePicker.setTitle("Select Time")
        mTimePicker.show()
        }

    override fun onBackPressed() {
        super.onBackPressed()
        totala = 0
    }
}

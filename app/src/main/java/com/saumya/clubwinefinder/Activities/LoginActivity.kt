package com.saumya.clubwinefinder.Activities

//import android.support.v7.app.AppCompatActivity
import android.annotation.TargetApi
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.animation.AlphaAnimation
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.saumya.clubwinefinder.AllLocationActivities.UsersCurrentLocation
import com.saumya.clubwinefinder.ModelClass.Error
import com.saumya.clubwinefinder.RetrofitApi.ApiClient
import com.saumya.clubwinefinder.fcm.MySingleton
import com.saumya.clubwinefinder.storage.SharedPrefManager
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginActivity : AppCompatActivity() {

    //---------------------------------This is signUp Activity----------------------------------------
    private var phonenumber: String = null.toString()

    //    val code = 1000
//    val firebaseUser = FirebaseAuth.getInstance().currentUser
    @TargetApi(Build.VERSION_CODES.O)
    private val buttonClick = AlphaAnimation(2f, 0.4f)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.saumya.clubwinefinder.R.layout.activity_login)
        btnSignUp.isFocusable = false
        btnSignUp.isClickable = false
        btnLogin.isFocusable= false
        btnLogin.isClickable=false

        btnSignUp.setOnClickListener {
            it.startAnimation(buttonClick)
            signUp()
            btnSignUp.isFocusable = true
            btnSignUp.setBackgroundColor(Color.GREEN)
//            startActivity(Intent(baseContext,NavigationActivity::class.java))
        }
        btnLogin.setOnClickListener {
            it.startAnimation(buttonClick)
            btnLogin.requestFocus()

            btnLogin.setBackgroundColor(Color.GREEN)
            startActivity(Intent(baseContext, NavigationActivity::class.java))
        }
    }

//    override fun onStart() {
//        //It is called after onCreate is called
//        super.onStart()
//        if (SharedPrefManager.getInstance(this).isLoggedIn) {
//            Log.e("onStart", "Inside onStart " )
//            btnLogin.visibility=View.VISIBLE
//            btnSignUp.visibility=View.INVISIBLE
//        }
//    }

    //For the first time user------
        private fun signUp() {

        phonenumber = etphone.text.toString()
        if (phonenumber.isEmpty()) {
            etphone.error = "Phone Number Is Required!"
            etphone.requestFocus()
            return
        }else
        if (!Patterns.PHONE.matcher(phonenumber).matches()) {
            etphone.error = "Enter Valid Phone Number"
            etphone.requestFocus()
            return
        } else if (phonenumber.length < 10) {
            etphone.error = "Phone Number Should Be 10 digit long"
            return
        }


        SharedPrefManager.getInstance(this).saveMOBILE(phonenumber)
        SharedPrefManager.getInstance(this).savephone(phonenumber)
        Log.d("AccountActivity", "phone in navi=" + SharedPrefManager.getInstance(this).getphone())
        val regi_path = "https://daru.club/api/register"
//        val requesta=Volley.newRequestQueue(this)
        val jsonabody = JSONObject()
        jsonabody.put("mobile", phonenumber)
        val requestbo = jsonabody.toString()
        // Request a string response from the provided URL.
        val stringRequest = object : StringRequest(
            Request.Method.POST, regi_path,
            com.android.volley.Response.Listener<String> { response ->
                // Display the first 500 characters of the response string.
                val jsons: JsonObject = JsonParser().parse(response).asJsonObject
                Log.d(ActualLogin.STRING_TAG, "json partser=" + jsons)

                val dataa = jsons.get("success").asBoolean
                val mess = jsons.get("message").asString
                val nama = jsons.get("name").toString()

                if (dataa) {
                    //-----------------success field returns true---------------------------
                    SharedPrefManager.getInstance(this).saveUser(nama)
                    SharedPrefManager.getInstance(this).savename(nama)

                    val intenta = Intent(this, OtpValidateActivity::class.java)
                    intenta.putExtra("phone", phonenumber)
                    intenta.putExtra("name", nama)

                    startActivity(intenta)
                }else{
                    Toast.makeText(baseContext, mess, Toast.LENGTH_SHORT).show()
                }


            },
            com.android.volley.Response.ErrorListener {
                Log.d(ActualLogin.STRING_TAG, "STRING error=" + it.localizedMessage)
            }) {

            override fun getBody(): ByteArray {
                return requestbo.toByteArray()
            }

            override fun getBodyContentType(): String {
                return "application/json"
            }

        }

        MySingleton.getInstance(this).addToRequestQueue(stringRequest)


    }
}

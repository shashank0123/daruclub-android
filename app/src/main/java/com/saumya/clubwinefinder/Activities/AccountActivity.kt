package com.saumya.clubwinefinder.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.saumya.clubwinefinder.fcm.MySingleton
import com.saumya.clubwinefinder.storage.SharedPrefManager
import org.json.JSONObject

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import com.payumoney.core.PayUmoneyConstants
import com.payumoney.core.PayUmoneySdkInitializer
import android.content.DialogInterface

import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager
import android.app.Activity

import android.app.AlertDialog
import android.content.Context
import android.os.Parcelable
import com.payumoney.core.PayUmoneyConfig
import com.payumoney.core.entity.TransactionResponse
import com.payumoney.core.request.PaymentRequest
import com.payumoney.sdkui.ui.utils.ResultModel
import com.saumya.clubwinefinder.R
import com.saumya.clubwinefinder.fcm.BaseApplication


class AccountActivity : AppCompatActivity() {

    companion object {
        val STRING_TAG = AccountActivity::class.java.simpleName
        var contexta: Context? = null
        var act: AccountActivity? = null
        var payment_hash = ""
        var get_merchant_ibibo_codes_hash = ""
        var vas_for_mobile_sdk_hash = ""
        var payment_related_details_for_mobile_sdk_hash = ""
        var verify_payment_hash = ""
        var delete_user_card_hash = ""
        var get_user_cards_hash = ""
        var edit_user_card_hash = ""
        var save_user_card_hash = ""
        var send_sms_hash = ""

        val Merchantkey = "zhr8UvQn"
        var Salt = "PSz3qDmg8a"
        val merchantid = "6088246"
        val taxid = "23423423"
        var phone = ""
    }

    private val mPaymentParams: PayUmoneySdkInitializer.PaymentParam? = null

    var amount: String? = null
    var productinfo: String? = null
    var firstname: String = ""
    var email: String? = null
    var user_credentials: String? = null
    var hasha: String? = null





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.saumya.clubwinefinder.R.layout.activity_account)


        if (PayUmoneyFlowManager.isUserLoggedIn(getApplicationContext())) {
//            logoutBtn.setVisibility(View.VISIBLE);
        } else {
//            logoutBtn.setVisibility(View.GONE);
        }

        amount = intent.getIntExtra("amount", 1).toString()
        email = intent.getStringExtra("email")
        productinfo = intent.getStringExtra("productinfo")
        firstname = intent.getStringExtra("firstname")
        user_credentials = intent.getIntExtra("user_credentials", 0).toString()


        contexta = this
        act = this
        phone = SharedPrefManager.getInstance(this).getphone()
        Log.d("AccountActivity", "phone in account=" + SharedPrefManager.getInstance(this).getphone())

        addapi()

    }

    fun addapi() {
        val regi_path = "https://daru.club/api/get_hash"

        val sd = SharedPrefManager.getInstance(this)
        val jsonabody = JSONObject()
        jsonabody.put("token", sd.gettoken())
        jsonabody.put("amount", amount)
        jsonabody.put("productinfo", productinfo)
        jsonabody.put("firstname", firstname)
        jsonabody.put("email", email)
        jsonabody.put("user_credentials", user_credentials)
        jsonabody.put("txnid", taxid)

        Log.d(STRING_TAG, "tokenaa=" + sd.gettoken())
        Log.d(STRING_TAG, "productinfo=" + productinfo)
        Log.d(STRING_TAG, "amount=" + amount)
        Log.d(STRING_TAG, "firstname=" + firstname)
        Log.d(STRING_TAG, "email=" + email)
        Log.d(STRING_TAG, "user_credentials=" + user_credentials)


        val requestbo = jsonabody.toString()
        // Request a string response from the provided URL.
        val stringRequest = object : StringRequest(
            Request.Method.POST, regi_path,
            com.android.volley.Response.Listener<String> { response ->
                // Display the first 500 characters of the response string.
                val jsons: JsonObject = JsonParser().parse(response).asJsonObject
                Log.d(STRING_TAG, "json partser=" + jsons)

                payment_hash = jsons.get("payment_hash").asString
                get_merchant_ibibo_codes_hash = jsons.get("get_merchant_ibibo_codes_hash").asString
                vas_for_mobile_sdk_hash = jsons.get("vas_for_mobile_sdk_hash").asString
                payment_related_details_for_mobile_sdk_hash =
                    jsons.get("payment_related_details_for_mobile_sdk_hash").asString

                verify_payment_hash = jsons.get("verify_payment_hash").asString
                delete_user_card_hash = jsons.get("delete_user_card_hash").asString
                get_user_cards_hash = jsons.get("get_user_cards_hash").asString
                edit_user_card_hash = jsons.get("edit_user_card_hash").asString
                save_user_card_hash = jsons.get("save_user_card_hash").asString
                send_sms_hash = jsons.get("send_sms_hash").asString



                setuppaymentparams()

            },
            com.android.volley.Response.ErrorListener {
                Log.d(STRING_TAG, "STRING error=" + it.localizedMessage)
            }) {


            override fun getBody(): ByteArray {
                return requestbo.toByteArray()
            }

            override fun getBodyContentType(): String {
                return "application/json"
            }

        }

        MySingleton.getInstance(this).addToRequestQueue(stringRequest)
    }


    fun setuppaymentparams() {


        val paymentaa = PayUmoneySdkInitializer.PaymentParam.Builder()
            .setAmount(amount)
            .setEmail(email)
            .setFirstName(firstname)
            .setProductName(productinfo)
            .setKey(Merchantkey)
            .setMerchantId(merchantid)
            .setIsDebug(true)
            .setTxnId(taxid)
            .setPhone(phone)
            .setsUrl("https://www.payumoney.com/mobileapp/payumoney/success.php")
            .setfUrl("https://www.payumoney.com/mobileapp/payumoney/failure.php")
            .build()

        Log.d(STRING_TAG, "paymenthash=" + payment_hash)
        paymentaa.setMerchantHash(payment_hash)

        PayUmoneyFlowManager.startPayUMoneyFlow(paymentaa, this, R.style.AppTheme_PopupOverlay, true)


    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        // Result Code is -1 send from Payumoney activity
        Log.d(STRING_TAG, "request code =" + requestCode + " resultcode =" + resultCode)
        if (requestCode === PayUmoneyFlowManager.REQUEST_CODE_PAYMENT && resultCode === Activity.RESULT_OK && data != null) {
            val transactionResponse = data.getParcelableExtra<TransactionResponse>(
                PayUmoneyFlowManager
                    .INTENT_EXTRA_TRANSACTION_RESPONSE
            )

            val resultModel = data.getParcelableExtra<ResultModel>(PayUmoneyFlowManager.ARG_RESULT)

            // Check which object is non-null
            if (transactionResponse != null && transactionResponse!!.getPayuResponse() != null) {
                if (transactionResponse!!.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
                    //Success Transaction

                    Toast.makeText(this, "Payment Sucessful", Toast.LENGTH_LONG).show()

                } else {
                    //Failure Transaction
                    finish()
                    Log.d(STRING_TAG, "Transaction failed ")
                    Toast.makeText(this, "Transaction failed", Toast.LENGTH_LONG).show()
                }

                // Response from Payumoney
                val payuResponse = transactionResponse!!.getPayuResponse()

                // Response from SURl and FURL
                val merchantResponse = transactionResponse!!.getTransactionDetails()

                Log.d(STRING_TAG, "payuResponse=" + payuResponse)
                Log.d(STRING_TAG, "merchantResponse=" + merchantResponse)

            } else if (resultModel != null && resultModel!!.getError() != null) {
                finish()
                Toast.makeText(
                    this, "Error response : " + resultModel!!.getError().getTransactionResponse(),
                    Toast.LENGTH_LONG
                ).show()

                Log.d(STRING_TAG, "Error response : " + resultModel!!.getError().getTransactionResponse())
            } else {
                Log.d(STRING_TAG, "Both objects are null!")
            }
        } else {
            Log.d(STRING_TAG, "error=" + resultCode)
        }
    }


//    private fun calculateServerSideHashAndInitiatePayment1(paymentParam: PayUmoneySdkInitializer.PaymentParam): PayUmoneySdkInitializer.PaymentParam {
//
//        val stringBuilder = StringBuilder()
//        val params = paymentParam.params
//        stringBuilder.append(params[PayUmoneyConstants.KEY] + "|")
//        stringBuilder.append(params[PayUmoneyConstants.TXNID] + "|")
//        stringBuilder.append(params[PayUmoneyConstants.AMOUNT] + "|")
//        stringBuilder.append(params[PayUmoneyConstants.PRODUCT_INFO] + "|")
//        stringBuilder.append(params[PayUmoneyConstants.FIRSTNAME] + "|")
//        stringBuilder.append(params[PayUmoneyConstants.EMAIL] + "|")
//        stringBuilder.append(params[PayUmoneyConstants.UDF1] + "|")
//        stringBuilder.append(params[PayUmoneyConstants.UDF2] + "|")
//        stringBuilder.append(params[PayUmoneyConstants.UDF3] + "|")
//        stringBuilder.append(params[PayUmoneyConstants.UDF4] + "|")
//        stringBuilder.append(params[PayUmoneyConstants.UDF5] + "||||||")
//
//        val appEnvironment = (application as BaseApplication).getAppEnvironment()
//        stringBuilder.append(appEnvironment.salt())
//
//        val hash = hashCal(stringBuilder.toString())
//        paymentParam.setMerchantHash(hash)
//
//        return paymentParam
//    }
//
//    fun launchapp() {
//        val payUmoneyConfig = PayUmoneyConfig.getInstance()
//        val builder = PayUmoneySdkInitializer.PaymentParam.Builder()
//    }
//
//
//
//
//
//    fun hashCal(str: String): String {
//        val hashseq = str.toByteArray()
//        val hexString = StringBuilder()
//        try {
//            val algorithm = MessageDigest.getInstance("SHA-512")
//            algorithm.reset()
//            algorithm.update(hashseq)
//            val messageDigest = algorithm.digest()
//            for (aMessageDigest in messageDigest) {
//                val hex = Integer.toHexString(0xFF and aMessageDigest.toInt())
//                if (hex.length == 1) {
//                    hexString.append("0")
//                }
//                hexString.append(hex)
//            }
//        } catch (ignored: NoSuchAlgorithmException) {
//        }
//
//        return hexString.toString()
//    }


}

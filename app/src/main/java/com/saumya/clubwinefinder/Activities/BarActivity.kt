package com.saumya.clubwinefinder.Activities

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.saumya.clubwinefinder.Adapter.ClubAdapter
import com.saumya.clubwinefinder.fcm.MyFirebaseMessagingService
import com.saumya.clubwinefinder.fcm.MySingleton
import com.saumya.clubwinefinder.storage.SharedPrefManager
import kotlinx.android.synthetic.main.activity_bar.*
import android.net.Uri
import com.saumya.clubwinefinder.R


class BarActivity : AppCompatActivity() {

    companion object {
        val STRING_TAG = BarActivity::class.java.simpleName
        var lati = 0.0
        var longi = 0.0
    }

    //------------------------This activity shows the BAR's Menu -----------------------------------------
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bar)

        val id = intent.getStringExtra("id")
        lati = intent.getDoubleExtra("lati", 0.0)
        longi = intent.getDoubleExtra("longi", 0.0)
        Log.d(STRING_TAG, "lati in bar=" + lati)
        Log.d(STRING_TAG, "longi in bar=" + longi)
        rvmenu.setHasFixedSize(true)
        rvmenu.layoutManager = LinearLayoutManager(this)
        val cc = "daru.club/api/barlist?lat&long&drink_type_id=1"
        var barurl = "https://daru.club/api/barlist"

        val builder = Uri.Builder()
        builder.scheme("https")
            .authority("daru.club")
            .appendPath("api")
            .appendPath("barlist")



        if (lati != 0.0) {
            builder.appendQueryParameter("lat", lati.toString())
        }

        if (longi != 0.0) {
            builder.appendQueryParameter("long", longi.toString())
        }

        builder.appendQueryParameter("drink_type_id", id)

        val myUrl = builder.build().toString()

        Log.d(STRING_TAG, "my url=" + myUrl)


        val hashMap = HashMap<String, String>()
        hashMap.put("id", id)
        hashMap.put("lat", lati.toString())
        hashMap.put("long", longi.toString())

        val detoken = SharedPrefManager.getInstance(this).getdevicetoken()
        val tokena = SharedPrefManager.getInstance(this).gettoken()

        if (detoken.length < 2 && tokena.length > 10) {
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
                MyFirebaseMessagingService().addapi(it.token, this)
                Log.d(STRING_TAG, "token=" + it.token)
            }
        }

        val stringRequest = object : StringRequest(
            Request.Method.GET, barurl,
            com.android.volley.Response.Listener<String> { response ->
                // Display the first 500 characters of the response string.
                val jsons: JsonObject = JsonParser().parse(response).asJsonObject
                Log.d(STRING_TAG, "json partser=" + jsons)

                val ss = jsons.get("status").asInt
                if (ss == 200) {
                    val barsa = jsons.getAsJsonArray("bars")
                    rvmenu.adapter = ClubAdapter(this, barsa)
                }


            },
            com.android.volley.Response.ErrorListener {
                Log.d(ActualLogin.STRING_TAG, "STRING error=" + it.localizedMessage)
            }) {
//            override fun getParams(): MutableMap<String, String> {
//                return hashMap
//            }

        }

        MySingleton.getInstance(this!!).addToRequestQueue(stringRequest)


        backbar.setOnClickListener {
            finish()
        }


    }








    }


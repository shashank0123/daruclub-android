package com.saumya.clubwinefinder.Activities

import android.content.Intent
//import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.iid.FirebaseInstanceId
import com.saumya.clubwinefinder.R
import com.saumya.clubwinefinder.fcm.MyFirebaseMessagingService
import com.saumya.clubwinefinder.storage.SharedPrefManager

class MainActivity : AppCompatActivity() {

    companion object {
        val STRING_TAG = MainActivity::class.java.simpleName
    }

    private var handler: Handler? =null
    private val splashlength:Long=2000
    private val handlerRunnable:Runnable= Runnable {
        if(!isFinishing){

            val tokena = SharedPrefManager.getInstance(this).gettoken()

            if (tokena.length > 10) {
                startActivity(Intent(baseContext, NavigationActivity::class.java))
            } else startActivity(Intent(baseContext, LoginActivity::class.java))

//            startActivity(Intent(baseContext, LoginActivity::class.java))

            finish()
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val detoken = SharedPrefManager.getInstance(this).getdevicetoken()
        val tokena = SharedPrefManager.getInstance(this).gettoken()

        if (detoken.length < 2 && tokena.length > 10) {
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {


                MyFirebaseMessagingService().addapi(it.token, this)
                Log.d(STRING_TAG, "token=" + it.token)


            }
        }

        handler=Handler()
        //----------------------postdelayed method for splash activity-------------------------------------
        handler!!.postDelayed(handlerRunnable,splashlength)
    }

    override fun onDestroy() {
        if (handler!=null){
            handler!!.removeCallbacks(handlerRunnable)
        }
        super.onDestroy()

    }
}

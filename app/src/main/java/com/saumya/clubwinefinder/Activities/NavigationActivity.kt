package com.saumya.clubwinefinder.Activities

import android.annotation.SuppressLint
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.google.firebase.iid.FirebaseInstanceId
import com.saumya.clubwinefinder.Fragments.BarFragment
import com.saumya.clubwinefinder.Fragments.DrinkTypeFragment
import com.saumya.clubwinefinder.R
import com.saumya.clubwinefinder.fcm.MyFirebaseMessagingService
import com.saumya.clubwinefinder.fcm.Utillity
import com.saumya.clubwinefinder.storage.SharedPrefManager
import com.tbuonomo.morphbottomnavigation.MorphBottomNavigationView
import kotlinx.android.synthetic.main.activity_bottom.*
import kotlinx.android.synthetic.main.activity_navigation.*
import kotlinx.android.synthetic.main.app_bar_navigation.*
import kotlinx.android.synthetic.main.app_bar_navigation.toolbar

class NavigationActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {


    companion object {
        val STRING_TAG = NavigationActivity::class.java.simpleName
        var lati = 0.0
        var longi = 0.0
    }

    private fun showFragment(){

        supportFragmentManager.beginTransaction().replace(R.id.fragcontiner, DrinkTypeFragment()).commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)


        val navView: MorphBottomNavigationView = this.findViewById(R.id.bottom_view)


        navView.setOnClickListener {
            val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START)
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }
        nav_view.setNavigationItemSelectedListener(this)

        nav_view.setNavigationItemSelectedListener(this)

        val detoken = SharedPrefManager.getInstance(this).getdevicetoken()
        val tokena = SharedPrefManager.getInstance(this).gettoken()

        if (detoken.length < 2 && tokena.length > 10) {
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
                MyFirebaseMessagingService().addapi(it.token, this)
                Log.d(MainActivity.STRING_TAG, "token=" + it.token)
            }
        }

//        navView.setOnClickListener {
//            val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
//            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
//                drawerLayout.closeDrawer(GravityCompat.START)
//            } else {
//                drawerLayout.openDrawer(GravityCompat.START)
//            }
//        }
//        nav_view.setNavigationItemSelectedListener(this)


        val ss = Utillity(this).is_location_permission(100)

        if (ss) {
            locati()
        }



    }

    override fun onStart() {
        super.onStart()

        Log.d(STRING_TAG, "acces token=" + SharedPrefManager.getInstance(this).gettoken())


        if ( !SharedPrefManager.getInstance(this).isLoggedIn){
            val intent=Intent(this, LoginActivity ::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        } else {
            showFragment()
        }
    }
    override fun onBackPressed() {
        AlertDialog.Builder(this)
            .setTitle("Really Exit?")
            .setMessage("Are you sure you want to exit?")
            .setNegativeButton(android.R.string.no, null)
            .setPositiveButton(android.R.string.yes, object : DialogInterface.OnClickListener{
                override fun onClick(dialog: DialogInterface?, which: Int) {

                    onBackPressed()
                }
            })
            .create()
            .show()
            finish()
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.navigation, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        when (item.itemId) {

        }
        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                supportFragmentManager.beginTransaction().replace(R.id.fragcontiner, DrinkTypeFragment()).commit()
            }

            R.id.nav_gallery -> {
                val intenta = Intent(this, SearchTabbedActivity::class.java)
                startActivity(intenta)
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 100) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                locati()
            }

        }
    }


    @SuppressLint("MissingPermission")
    fun locati() {

        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationClient.lastLocation.addOnSuccessListener {

            if (it != null) {
                lati = it.latitude
                longi = it.longitude


            }

            Log.d(STRING_TAG, "lati=" + lati)
            Log.d(STRING_TAG, "longi=" + longi)
        }
    }
}

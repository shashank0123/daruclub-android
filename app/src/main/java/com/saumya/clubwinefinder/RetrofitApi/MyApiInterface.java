package com.saumya.clubwinefinder.RetrofitApi;


import com.saumya.clubwinefinder.ModelClass.*;
import com.saumya.clubwinefinder.ModelClass.Error;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

//------------------Interface Class---------------------
public interface MyApiInterface {

//-------In this interface we make call and mention the fields which we will post or get in the API-------------

    @FormUrlEncoded
    @POST("register")
    Call<Error> createUser(
            @Field("mobile") String mobile
    );

    //----------------------In bracket after keyword GET or POST use same name as the ENDAPIKEY------------------------
    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> userLogin(
            @Field("mobile") String mobile
    );

    //----------------------In bracket after keyword GET or POST use same name as the ENDAPIKEY------------------------
    //----------------------get request for google nearaby API ----------------------------




    @GET("barlist")
    Call<BarsItem> getmenu();

    @GET("bardetails")
    Call<BarDetails> getBarDetail();

    @GET("menu_items")
    Call<MenuBarsItem> getDetailMenu();

}

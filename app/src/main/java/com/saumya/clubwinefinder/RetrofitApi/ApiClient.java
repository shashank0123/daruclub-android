package com.saumya.clubwinefinder.RetrofitApi;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    //------------------Client is for getting the base url , instance , retrofit object and then getApi()------------
    private static final String Baseurl = "https://daru.club/api/";

    // -------------Another base url-----------------------------
    private static final String MapBaseUrl="https://maps.googleapis.com/maps/";

    //Instance Is created of the same class-----------
    private static ApiClient mInstance;

    //Retrofit Object is created----------------
    private Retrofit retrofit;
    private Retrofit  mapretrofit;
    //constructor------
    private ApiClient() {

        retrofit = new Retrofit.Builder().baseUrl(Baseurl).addConverterFactory(GsonConverterFactory.create()).build();
        mapretrofit=new Retrofit.Builder().baseUrl(MapBaseUrl).addConverterFactory(GsonConverterFactory.create()).build();

    }

    //method to get instance of the class
    public static synchronized ApiClient getInstance() {
        if (mInstance == null) {
            mInstance = new ApiClient();
        }
        return mInstance;
    }

    //Can be used in other classes to call or get the API
    public MyApiInterface getApi() {
        return retrofit.create(MyApiInterface.class);
    }
    public MyApiInterface getMapApi(){return  mapretrofit.create(MyApiInterface.class); }



}

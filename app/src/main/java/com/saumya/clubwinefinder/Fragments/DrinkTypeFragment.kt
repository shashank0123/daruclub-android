package com.saumya.clubwinefinder.Fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.saumya.clubwinefinder.Adapter.DrinkTypeAdapter

import com.saumya.clubwinefinder.R
import com.saumya.clubwinefinder.fcm.MySingleton
import kotlinx.android.synthetic.main.fragment_drink_type.view.recycle_drinktype


/**
 * A simple [Fragment] subclass.
 */
class DrinkTypeFragment : Fragment() {

    companion object {
        val STRING_TAG = DrinkTypeFragment::class.java.simpleName
        var rec: RecyclerView? = null
    }

    var viewa: View? = null
    val drinktypeurl = "https://daru.club/api/drink_type"
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        viewa = inflater.inflate(R.layout.fragment_drink_type, container, false)
        viewa!!.recycle_drinktype.setHasFixedSize(true)
        viewa!!.recycle_drinktype.layoutManager = GridLayoutManager(context, 2)
        rec = viewa!!.recycle_drinktype

        val stringRequest = object : StringRequest(
            Request.Method.GET, drinktypeurl,
            com.android.volley.Response.Listener<String> { response ->
                // Display the first 500 characters of the response string.
                val jsons: JsonObject = JsonParser().parse(response).asJsonObject
                Log.d(STRING_TAG, "json partser=" + jsons)

                val drintype = jsons.getAsJsonArray("drink_type")
                Log.d(STRING_TAG, "json drinktype=" + drintype)
//                val contexta=context as DrinkTypeFragment

                if (DrinkTypeFragment != null) {
                    DrinkTypeFragment.rec!!.adapter =
                        DrinkTypeAdapter(context!!, drintype)
                }


            },
            com.android.volley.Response.ErrorListener {
                Log.d(STRING_TAG, "STRING error=" + it.localizedMessage)
            }) {


        }

        MySingleton.getInstance(context!!).addToRequestQueue(stringRequest)



        return viewa
    }

}// Required empty public constructor

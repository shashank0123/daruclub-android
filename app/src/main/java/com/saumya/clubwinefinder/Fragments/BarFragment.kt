package com.saumya.clubwinefinder.Fragments


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.saumya.clubwinefinder.Activities.ActualLogin
import com.saumya.clubwinefinder.Activities.NavigationActivity
import com.saumya.clubwinefinder.Adapter.ClubAdapter
import com.saumya.clubwinefinder.ModelClass.BarsItem
import com.saumya.clubwinefinder.R
import com.saumya.clubwinefinder.RetrofitApi.ApiClient
import com.saumya.clubwinefinder.fcm.MySingleton
import com.saumya.clubwinefinder.storage.SharedPrefManager
import kotlinx.android.synthetic.main.fragment_bar.*
import retrofit2.Call
import retrofit2.Response


/**
 * A simple [Fragment] subclass.
 */
class BarFragment : Fragment() {

    var myarray= arrayListOf<BarsItem>()
    val drinktypeurl = "daru.club/api/drink_type"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val recyclerView = getView()?.findViewById<RecyclerView>(R.id.rvclub)
        rvclub.layoutManager=LinearLayoutManager(requireContext())
//        val clubAdapter = ClubAdapter(requireContext() , myarray)
//        rvclub.adapter = clubAdapter


        val stringRequest = object : StringRequest(
            Request.Method.POST, drinktypeurl,
            com.android.volley.Response.Listener<String> { response ->
                // Display the first 500 characters of the response string.
                val jsons: JsonObject = JsonParser().parse(response).asJsonObject
                Log.d(ActualLogin.STRING_TAG, "json partser=" + jsons)

                val drintype = jsons.getAsJsonArray("drink_type")

            },
            com.android.volley.Response.ErrorListener {
                Log.d(ActualLogin.STRING_TAG, "STRING error=" + it.localizedMessage)
            }) {


        }

        MySingleton.getInstance(context!!).addToRequestQueue(stringRequest)














        val call =ApiClient.getInstance().api.getmenu()
        call.enqueue(object : retrofit2.Callback<BarsItem>{
            override fun onFailure(call: Call<BarsItem>, t: Throwable) {
                t.printStackTrace()
                Log.d("onFail", t.printStackTrace().toString())
            }

            override fun onResponse(call: Call<BarsItem>, response: Response<BarsItem>) {

                Log.d("onResponse" , response.body().toString())

                val newresponse= response.body()

                myarray.add(BarsItem(response.body()!!.name,response.body()!!.image, response.body()!!.rating))
//               clubAdapter.notifyDataSetChanged()
            } })


    }
}
